
\documentclass{amsart}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[colorlinks,backref,bookmarks]{hyperref}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,shapes}
\usepackage[section]{placeins}
\usepackage[mathscr]{euscript}
\usepackage[notref,notcite]{showkeys}		% for editing
\newcommand{\comment}[1]{\textcolor{blue}{#1}}	% for editing
\usepackage{enumitem}
\usepackage[all,cmtip]{xy}

\linespread{1.2}
\renewcommand{\arraystretch}{1.2}

\newtheorem{lemma}{Lemma}[section]
\newtheorem{theorem}[lemma]{Theorem}
\newtheorem{proposition}[lemma]{Proposition}
\newtheorem{corollary}[lemma]{Corollary}
\newtheorem{claim}[lemma]{Claim}
\theoremstyle{definition}
\newtheorem{example}[lemma]{Example}
\newtheorem{remark}[lemma]{Remark}
\newtheorem*{notation}{Notation}
\newtheorem{definition}[lemma]{Definition}
\newtheorem{question}[lemma]{Question}
\newtheorem{observation}[lemma]{Observation}
\newtheorem{fact}[lemma]{Fact}

\newcommand{\G}[1][n]{\mathscr{G}_{#1}}		% wreath product
\newcommand{\A}[1][n]{\mathscr{A}_{#1}}		% arrangement
\newcommand{\M}[1][n]{\mathscr{M}_{#1}}		% complement
\renewcommand{\P}[1][n]{\mathscr{P}_{#1}}	% poset of layers
\newcommand{\D}[1][n]{\mathscr{D}_{#1}}		% Dowling poset
\newcommand{\Q}{\mathscr{Q}}			% partition lattices
\renewcommand{\O}{\mathscr{O}}			% orbit set
\newcommand{\ZZ}{\mathbb{Z}}			% integers
\newcommand{\Z}{\mathbb{Z}}			% integers option 2
\newcommand{\QQ}{\mathbb{Q}}			% rationals
\newcommand{\rat}{\mathbb{Q}}			% rationals option 2
\newcommand{\CC}{\mathbb{C}}			% complex numbers
\newcommand{\C}{\mathbb{C}}			% complex numbers option 2
\renewcommand{\AA}{\mathbb{A}}			% affine space
\newcommand{\RR}{\mathbb{R}}			% real numbers
\newcommand{\R}{\mathbb{R}}			% real numbers option 2

\newcommand{\Sn}[1][n]{\mathscr{S}_{#1}}	% symmetric group
\newcommand{\sym}[1][n]{\mathfrak{S}_{#1}}	% symmetric group option 2
\newcommand{\n}[1][n]{\mathbf{#1}}		% set {1,2,...,n}
\newcommand{\Ginj}[1][f]{\overline{\mathbf{#1}}}	% G-injection (FI_G morphism)
\newcommand{\F}{X^{(G)}}			% non-free pointskj
\newcommand{\oo}{\mathfrak{o}}			% an orbit

\newcommand{\FI}{\mathtt{C}}			% an FI-type category
\newcommand{\nn}[1][n]{\mathtt{#1}}		% category object
\newcommand{\fig}{\mathrm{FI}_G}		% FI_G category
\newcommand{\Arr}{\mathtt{Arr}}			% Arr category of arrangements
\newcommand{\Pos}{\mathtt{Pos}}			% Pos category of posets
\newcommand{\Ab}{\mathtt{Ab}}			% Ab category of abelian groups
\newcommand{\Set}{\mathtt{Set}}			% SET category of sets

\newcommand{\zero}{\hat{0}}			% minimum of poset
\newcommand{\onehat}{\hat{1}}
\renewcommand{\H}{\widetilde{H}}			% Whitney homology
\DeclareMathOperator{\WH}{WH}			% Whitney homology
\DeclareMathOperator{\Ho}{H}			% homology/cohomology
\newcommand{\redH}{\widetilde{\Ho}}		% reduced homology

\newcommand{\wt}[1]{\widetilde{#1}}		% enriched partition
\newcommand{\tuple}[1][\beta]{\langle #1 \rangle}	% tuple of partitions
\newcommand{\parts}[1][\beta]{(\wt{#1},z)}%	more tuples

\newcommand{\into}{\hookrightarrow}		% injection

\newcommand{\ip}{+}						% for small coproducts.

\DeclareMathOperator{\Conf}{Conf}		% config space
\DeclareMathOperator{\Aut}{Aut}			% automorphisms
\DeclareMathOperator{\codim}{codim}		% codimension
\DeclareMathOperator{\rk}{rk}			% rank
\DeclareMathOperator{\Ind}{Ind}			% Induced representations
\DeclareMathOperator{\im}{im}			% image of a map
\DeclareMathOperator{\Hom}{Hom}

\title{Arrangements with group actions and representation stability}
\author{Christin Bibby}
\address{Department of Mathematics, University of Michigan, Ann Arbor, MI, USA}
\email{\href{mailto:bibby@umich.edu}{bibby@umich.edu}}
\author{Nir Gadish}
\address{Department of Mathematics, University of Chicago, Chicago, IL, USA}
\email{\href{mailto:nirg@uchicago.edu}{nirg@uchicago.edu}}

\keywords{Dowling lattice, hyperplane arrangement, representation stability, ...?}
\subjclass[2010]{05E18, 52C35, 06A07, 20C30, ...?}

\begin{document}

\begin{abstract}
	From a group action on a variety, define a variant of the configuration space by insisting that no two points inhabit the same orbit. When the action is almost free, this "orbit configuration space" is the complement of an arrangement of subvarieties inside the cartesian product, and we use this structure to study its topology.

	We give an abstract combinatorial description of its poset of layers (connected components of intersections from the arrangement) which turns out to be of much independent interest as a generalization of partition and Dowling lattices. The close relationship to these classical posets is then exploited to give explicit cohomological calculations akin to those of (Totaro '96).
	
	Lastly, the wreath product of the group acts naturally. We study the induced action on cohomology using the language of representation stability: considering the sequence of all such arrangements and maps between them, the sequence of representations stabilizes in a precise sense. This is a consequence of combinatorial stability at the level of posets.
\end{abstract}

\maketitle
\tableofcontents


\section{Representation stability}\label{sec:repstable}

The arrangements considered above become increasingly complicated as the
parameter $n$ grows. Yet considering all of them at the same time gives the
problem extra structure -- this is the topic of this section. Recent years
brought about a new approach to organizing the combinatorics of families
arrangements, as the ones studied above: this is the framework of representation
stability, initiated by \cite{CEF2015} and expanded to the relevant context in
\cite{Gadish2016}. One applies this set of ideas by first collecting a family
of related arrangements into a single object -- a $\FI$-arrangement,
parameterized by some category $\FI$ (see \cite{Gadish2016}). It turns out that the combinatorics of the arrangements is controlled by the structure of $\FI$, with concrete implications to the representation occurring in the cohomology of their complements. This structure is present in the context of orbit configuration spaces, as we now demonstrate.

\subsection{$\fig$-Arrangements}\label{sec:FIA}
Let $G$ be a finite group as above, and let $X$ be a \underline{connected}
almost free $G$-manifold (or variety) of dimension $\geq 2$. The idea that the
sequence of arrangements $\{\A(X,G)\}_{n\leq 1}$ form a single structured object
is formalized using the language of category theory. The particulars of the
general construction, e.g. those defining the \emph{category of arrangements},
will not be needed in their full generality. Instead of presenting them here, we
chose focus only on the concrete example of $\A(X,G)$. See \cite{Gadish2016} for the general construction of in the case of linear subspace arrangements.

A method of assembling a collection of arrangements and maps between them into a mathematical object proceeds by considering a functor $\A[{\bullet}]$ from some parameterizing category $\FI$ to the category of arrangements $\Arr$ (whose exact definition will not be important here and thus omitted).
\begin{definition}[\textbf{$\FI$-arrangements}]
A functor $\A[\bullet]: \FI \rightarrow \Arr$ is called a \emph{$\FI$-arrangement}.
\end{definition}
Through such an object, every object $\nn[c]$ of $\FI$ corresponds to an arrangement $\A[{\nn[c]}]$, and every morphism $\nn[c]\rightarrow \nn[d]$ induces a map of arrangements $\A[{\nn[c]}]\rightarrow \A[{\nn[d]}]$ (which, again, we will not define generally -- see the example below).

To apply this idea to the sequence $\{\A(X,G)\}_{n\geq 1}$ consider the following indexing category.
\begin{definition}[\textbf{The category $\fig$} \cite{SS2016b}, \cite{GL2015}]
Let $\fig$ be the category whose objects are finite sets, and whose morphisms $\Hom_{\fig}(S,T)$ are the so called \emph{$G$-injections}, i.e. pairs $(\bar{g},f)$ where $f: S\hookrightarrow T $ is an injection, and  $\bar{g}:S\rightarrow G$ is any function.

The composition of two $G$-injections is given by
$$
(\bar{g},f)\circ (\bar{g}',f') = ((\bar{g}\circ f)\cdot\bar{g}' ,f\circ f')
$$
where $(\bar{g}\circ f)\cdot\bar{g}'$ is the pointwise multiplication of the two $G$-valued functions on $S$. In everything that follows we abbreviate $(\bar{g},f)$ to $\Ginj$.

\end{definition}

Note that every object in $\fig$ is isomorphic to one of
$$
[\nn] = \{ 1,2,\ldots, n \} \quad \text{for } n\in \mathbb{N}
$$
and thus it suffices to consider these objects in a description of any functorial construction. We will denote the image of $[\nn]$ under a functor $F_\bullet$ by $F_{\nn}$. Further note that the endomorphisms of $[\nn]$ are precisely the group $\G[n]=G\wr\Sn$. To produce an $\fig$-arrangement from the $\A(X,G)$'s, start with the following (contravariant) functor:
$$
X^\bullet: [\nn] \mapsto X^{[\nn]} = \operatorname{Func}([\nn],X) \cong X^n.
$$
A $G$-injection $(\bar{g},f):[\nn]\rightarrow [\nn[m]]$ acts on $X^\bullet$ naturally by
$$
(\bar{g},f)^* : (x_1,\ldots,x_m) \mapsto ((g_1)^{-1}\cdot x_{f(1)},\ldots ,(g_n)^{-1}\cdot x_{f(n)}) \in X^n.
$$
More abstractly, $(\bar{g},f)$ acts on a function $\bar{x}:[\nn[m]]\rightarrow X$ by first precomposing $\bar{x}\circ f$, then acting by $\bar{g}^{-1}\curvearrowright X^n$.

Next consider the arrangements $\A[](X,G)$. Observe that the generating subspaces $H_{ij}(h)$ and $H_i^s$ of $\A(X,G) \subseteq X^n$, defined in \S\ref{sec:arrangementdefs}, pull back to generating subspaces of $\A[m](X,G)\subseteq X^m$ under the preimage of $(\bar{g},f)^*$. To abbreviate, we will denote the preimage $\left((\bar{g},f)^*\right)^{-1}$ by a suggestive push-forward notation $(\bar{g},f)_*$. Indeed, elementary verification shows:
\begin{claim}[\textbf{Push-forward of generating subspaces}]\label{claim:push-forward}
\begin{eqnarray}
(\bar{g},f)_*: H_{ij}(h) \mapsto H_{f(i),f(j)}(g_j^{-1} h g_i), \\ 
(\bar{g},f)_*: H_{i}^{s} \mapsto H_{f(i)}^{g_i^{-1}\cdot s}.
\end{eqnarray}
\end{claim}
A map of this form between two arrangements, i.e. one that sends every distinguished subspaces in the domain to another in the range, is what we call a \emph{morphism of arrangements}. Since we are interested only in the explicit example given above, we will not define this notion any more carefully. However, with this terminology it is now clear that the collection $\{\A(X,G)\}_{n\geq 1}$ can be given the structure of a $\fig$-arrangement:
\begin{definition}[\textbf{The $\fig$-arrangement $\A[\bullet](X,G)$}]
Let $\A[\bullet](X,G)$ be the functor $\fig \rightarrow \Arr$ that sends every finite set $[\nn]$ to the arrangement $\A[{[\nn]}](X,G) \subseteq X^{[\nn]}$, and every $G$-injection $\Ginj:[\nn] \rightarrow [\nn[m]]$ to the arrangement map $\Ginj_*$ as constructed above.
\end{definition}

\begin{remark}[\textbf{Right vs. left actions}]
At this point the reader should notice that the action of $\G$ on $\A(X,G)$ that emerges here does \emph{not} coincide with the action as defined in \S\ref{sec:arrangementdefs}! There, we defined a \emph{left} action on the points of the arrangements, while here we get a \emph{right} action. This is a reflection of the fact that the category $\fig$ acts on $X^\bullet$ naturally contravariantly. When dealing with a single value of $n$, one could simply convert the right action into a left one by setting $g.x := x.g^{-1}$, thus arriving at the definition appearing in \S\ref{sec:arrangementdefs}. However, when $n$ is allowed to vary, it becomes clear that the natural action is a right one. We chose to use the left action in our exposition above in keeping with traditional convention.

With this understood, consider the induced action on the hypersurfaces $H_{ij}(g)$ and $H_i^s$ generating $\A(X,G)$. Observe that the action in \S\ref{sec:arrangementdefs} is defined by taking their direct image, while the natural action in the current context is by considering preimages (e.g. since the preimage of a hypersurface is again a hypersurface, while its image is generally \emph{not}). The passage to taking preimages reverses the direction of the natural right action, thus inducing the standard left action on the generating hypersurfaces. It follows that there is \emph{no difference} between the two actions at the level of the posets $\P(X,G)$.
\end{remark}

To every arrangement $\A(X,G)$ one can apply the functor that sends it to its poset of layers $\P(X,G)$. Composing this functor with the $\fig$-arrangement $\A[\bullet](X,G)$ produces now a functor 
$$
\P[\bullet](X,G): \fig \rightarrow \Pos
$$
landing in the category of posets, denoted by $\Pos$. A $G$-injection $\Ginj$ defines a push-forward map between posets as described in Claim \ref{claim:push-forward}, and extended to all intersections naturally.

\begin{remark}\comment{perhaps not necessary}
Under the map induced by $\Ginj^*:X^{[\nn[m]]}\rightarrow X^{[\nn]}$, the preimage of any connected subspace is again connected (hence the reason we restrict attention to a connected space $X$). This observation ensures that the push-forward map $\Ginj_*:\P(X,G)\rightarrow \P[m](X,G)$ is well defined on the layers, which are connected components.
\end{remark}


\subsection{Stable $\fig$-posets}
The benefit of the functorial approach is that at the level of $\fig$-posets stability phenomena emerge:
\begin{definition}[\textbf{Combinatorial stability of $\fig$-posets} \cite{Gadish2016}]
Let $\P[\bullet]$ be an $\fig$-poset.
\begin{itemize}
\item $\P[\bullet]$ is \emph{finitely-generated} if for every fixed rank $r$ there exists a finite set $x_i\in \P[{\nn_i}]$ for $i=1,\ldots,d$ whose orbits under the morphisms of $\fig$ cover all rank $r$ elements of $\P[\bullet]$. Explicitly, for every $y\in \P[{\nn[m]}]$ of rank $r$ there exists $1\leq i \leq d$ and a morphism $\Ginj:\nn[n]_i\rightarrow \nn[m]$ with $\Ginj_*(x_i)=y$.
\item $\P[\bullet]$ is \emph{downward stable} if for every $x\in \P[{\nn}]$ and for every $G$-injection $\Ginj:\nn\rightarrow\nn[m]$, the map induced on lower intervals $\Ginj_*: \left(\hat{0},x\right) \rightarrow \left(\hat{0},\Ginj_*(x)\right) \subset \P[{\nn[m]}]$ is bijective.
\end{itemize}
An $\fig$-poset satisfying both properties is said to be \emph{combinatorially stable}. 
\end{definition}
The significance of combinatorial stability is in its homological consequences:
\begin{fact}[{\textbf{Homological representation stability} \cite[Theorem
??]{Gadish2016}}]
If $\P[\bullet]$ is a combinatorially stable, ranked $\fig$-poset, then its Whitney homology $\fig$-groups
$$
\WH_i(\P[\bullet]):\fig \rightarrow \Ab \qquad \nn \mapsto \WH_i(\P[{\nn}]) :=
\bigoplus_{x\in \P[{\nn}]^{i}} \H_{i-2}((\zero, x);\ZZ)
$$
are a finitely generated $\fig$-module for every $i\geq 0$. Equivalently, they exhibits representation stability in the sense of \cite[Definition ??]{SS2016b} (see \cite[Theorem ??]{SS2016b} for a proof of the equivalence).
\end{fact}

We will see below that for posets that arise from arrangements, such as $\P(X,G)$, a much stronger statement can be made. To state the result we need further definitions.
\begin{definition}[Induced and projective $\fig$-posets]
An $\fig$-set $S_\bullet:\fig\rightarrow \Set$ is \emph{induced} of degree $\nn$ if there exists a natural isomorphism 
$$
S_{\bullet} \cong \operatorname{Hom}_{\fig}(\nn,\bullet)\times_{\G} S_{\nn}.
$$
The $\fig$-set is said to be \emph{projective} if it is the disjoint union of induced $\fig$-sets (of various degrees). Lastly, an $\fig$-poset $\P[\bullet]$ is said to be \emph{projective} if the underlying $\fig$-set obtained by forgetting the ordering is projective.
\end{definition} 
As above, this property has immediate homological consequences:
\begin{theorem}[\textbf{Homological projectivity}]\label{thm:homological_projectivity}
If $\P[\bullet]$ is projective and downward stable, then its Whitney homology $\WH(\P[\bullet])$ is
a projective $\fig$-module\footnote{Such $\fig$-modules are called \emph{free}
in \cite{Gadish2017}. This use of ``projective" coincides with the standard usage in algebra.}.
\end{theorem}
Projective $\fig$-modules are a special case of the objects studied in \cite{Gadish2017}, where many representation-theoretic implications of projectivity are enumerated. In particular, explicit consequences are part of Theorem ?? \comment{content of theorem in the intro}. 
\begin{proof}[Proof of Theorem \ref{thm:homological_projectivity}]
Suppose that after forgetting the ordering, $\P[\bullet]$ is a projective $\fig$-set. Applying the $i$-th Whitney homology functor produces $\fig$-abelian groups
\begin{equation}\label{eq:Whitney_decomposition}
\WH(\P[\bullet])_i = \oplus_{x\in \P[\bullet]^i} \H_{i-2}((\hat{0},x);\ZZ).
\end{equation}
By downward stability every $G$-injection $\Ginj:\nn \rightarrow \nn[m]$ induces an isomorphism of posets
\[\Ginj_*: (\hat{0},x) \rightarrow (\hat{0},\Ginj_*(x))\]
and thus an isomorphism of the corresponding summands 
\[\H_{i-2}(\hat{0},x)\rightarrow \H_{i-2}(\hat{0},\Ginj_*(x)).\]
Recall that a $G$-representation that decomposes as $\oplus_{x\in X}V_x$, where the summands get permuted by the $G$-action, is induced from a representation of a subgroup. The description of $\WH(\P[\bullet])$ in Equation \ref{eq:Whitney_decomposition} is similar, and therefore one wishes to say that it is an induced $\fig$-module. Indeed, the next lemma shows that it is the case.
\end{proof}

\begin{lemma}[\textbf{Induction characterization}]
Suppose $S_\bullet = \Hom_{\fig}(\nn[m],\bullet)\times_{\G[m]} S_{\nn[m]}$ is an induced $\fig$-set of degree $\nn[m]$, generated by a transitive $\G[m]$-set $S_{\nn[m]}$. If $M_\bullet$ is an $\fig$-module of the form
$$
M_{\nn} = \bigoplus_{s\in S_{\nn}} V_s
$$
and every $G$-injection $\Ginj:\nn_1\rightarrow\nn_2$ acts by sending $V_s$ to $V_{\Ginj_*(s)}$ isomorphically, then $M_\bullet$ is an induced $\fig$ module of degree $\nn[m]$, that is
$$
M_\bullet \cong \Hom_{\fig}(\nn[m],\bullet) \times_{\G[m]} M_{\nn[m]}.
$$
To give a slightly more refined statement: if $H_{s_0}\leq \G[m]$ is the stabilizer of some generator $s_0\in S_{\nn[m]}$, then 
$$
M_\bullet \cong \Hom_{\fig}(\nn[m],\bullet) \times_{H_{s_0}} V_{s_0}.
$$

Lastly, if $S_\bullet$ is a projective $\fig$-set, then an $\fig$-module $M_\bullet$ as above is projective.
\end{lemma}
\begin{proof}
The universal property characterizing $Q_\bullet:=\Hom_{\fig}(\nn[m],\bullet) \times_{H_{s_0}} V_{s_0}$ states that maps $Q_\bullet \rightarrow N_\bullet$ are the same are $H_{s_0}$-linear maps $V_{s_0}\rightarrow N_{\nn[m]}$. Checking that $M_\bullet$ satisfies this property is routine. Next, observe that
\begin{eqnarray*}
\Hom_{\fig}(\nn[m],\bullet) \times_{H_{s_0}} V_{s_0} &\cong& \Hom_{\fig}(\nn[m],\bullet) \times_{\G[m]} \left(\G[m] \times_{H_{s_0}} V_{s_0}\right)\\ &\cong&  \Hom_{\fig}(\nn[m],\bullet) \times_{\G[m]} \left( \oplus_{s\in S_{\nn[m]}} V_s \right)
\end{eqnarray*}
which is the claimed result.

For a projective $\fig$-set $S_\bullet$, a decomposition into a disjoint union of transitive induced $\fig$-sets gives rise to a direct sum decomposition of $M_\bullet$ into components of the form given in the previous paragraph. Thus $M_\bullet$ is a direct sum of induced $\fig$-modules, parameterized by the transitive $\fig$-sets that make up $S_\bullet$.
\end{proof}

\subsection{Stability of $\P[\bullet](G,S)$}
\EEE Define truncations here and prove that they are initial in their orbits. This will be useful!

In the previous section we saw that the $\fig$-poset of layers of the $\fig$-arrangement $\A[\bullet](G,X)$ is made up of all the $S$-Dowling posets $\P(G,S)$ with their $\G$-action as $n$ ranges over the natural numbers (equivalently, the objects of $\fig$). Using the same rules as given in Claim \ref{claim:push-forward}, it is clear that, for every $G$-set $S$, the various $S$-Dowling posets can be given the structure of an $\fig$-poset: 
explicitly, a $G$-injection $\Ginj_*:\P[{\nn}] \rightarrow \P[{\nn[m]}]$ sends a partial $G$-partition of $\nn$ to one of the larger set $\nn[m]$ by first changing the $G$-labeling according to $\bar{g}$, then by sending elements of the existing blocks to their image under $f$, and lastly by partitioning all remaining $i\in \nn[m] \setminus f(\nn)$ into singletons (in which the $G$-labeling is meaningless).


We use the notation $\P[\bullet](G,S)$ for the $\fig$-poset that arises -- it is the Whitney homology of this object that we wish to study now. From the above discussion it follows that this Whitney homology exhibits representation stability (i.e., it is a combinatorially stable and projective $\fig$-module) if this $\fig$-poset is combinatorially stable and projective. This is indeed the case:
\begin{theorem}[\textbf{Stability of $\P[\bullet](G,S)$}]
For any finite group $G$ and a finite $G$-set $S$, the $\fig$-poset $\P[\bullet](G,S)$ is finitely generated, downward stable, and projective.
\end{theorem}
\begin{proof}
Let $(\wt{\beta},z)$ be a $G$-partition on $\nn$, and let $\beta = \{B_1,\ldots, B_k\}$ be the underlying set partition with $b_\ell:B_\ell\rightarrow G$ representatives of the projective $G$-coloring on $B_\ell$. We truncate this partition by removing all singleton blocks. Explicitly, label the blocks so that $B_{d+1},\ldots,B_k$ are all the singletons and consider the $G$-partition $\wt{\beta}^t = \{B_1,\ldots,B_d\}$ on the set
$$
\nn^t := \bigcup_{\ell=1}^d B_\ell \cup Z = \nn \setminus \bigcup_{\ell=d+1}^k B_\ell
$$
with $G$-colorings $b_\ell$ and $z$ as in $(\wt{\beta},z)$.

Define a $G$-injection $\Ginj^t$, called \emph{the truncation}, as follows: let $f^t:\nn^t\hookrightarrow \nn$ be the inclusion, and let $\bar{g} = \bar{1}$ the $G$-coloring that sends every element to $1$. As state above, a $G$-injection acts on a $G$-partition by adjoining the elements not hit by $f$ as singleton blocks. It follows that the map induced by the truncation
$$
 \Ginj^t_*: \P[\nn^t]\rightarrow \P[\nn]
$$
sends $(\wt{\beta}^t,z)$ to $(\wt{\beta},z)$.

To show that $\P[\bullet]$ exhibits downward stability, it will suffice to prove that $\Ginj^t_*$ induces a bijection $(\zero,(\wt{\beta}^t,z))\rightarrow (\zero,(\wt{\beta},z))$. But this is obvious: if $(\wt{\alpha},w)\leq (\wt{\beta},z)$ then the set partition $\alpha$ is a refinement of $\beta$, in particular all singletons of $\beta$ are singletons of $\alpha$. Thus an inverse map
$$
(\zero,(\wt{\beta}^t,z))\leftarrow (\zero,(\wt{\beta},z)): t
$$
is given by excising away the singletons $B_{d+1}\cup\ldots\cup B_k$.

Next, fix a rank $i$. We will show that if $(\wt{\beta},z)$ has this rank, then it is in the image of $\P[{\nn[m]}]$ with $m\leq 2i$. Since this is a finite list of finite posets, it will follow that the orbits of finitely many elements cover the rank $i$ part of $\P[\bullet]$. The rank of $(\wt{\beta},z)$ is equal to the number of equations needed to specify the relations among the coordinates of the corresponding layer, thus
$$
i = \sum_{\ell=1}^k (|B_\ell| - 1)+ |Z| = n-k.
$$
We arranged the blocks so that $B_\ell$ with $\ell > d$ are the singletons, removed by the truncation. Thus $\wt{\beta}^t$ is a partition of $\nn^t$ whose cardinality is
$$
n-(k-d) = i + d \leq 2i
$$
where the last inequality follows from the fact $i \geq \sum_{\ell=1}^d(|B_\ell| - 1)$ while $|B_\ell|-1 \geq 1$ for all $\ell \leq d$.
It follows that there is exists an isomorphism $\nn[m]\rightarrow\nn^t$ in  $\fig$ for some $m\leq 2i$, and therefore the truncation $(\wt{\beta}^t,z)$ belongs to an orbit of $\P[{\nn[m]}]$.

Lastly, to show that $\P[\bullet]$ is projective we use the important observation that the truncation is initial in the category of elements mapping to $(\wt{\beta},z)$. Explicitly,
\begin{claim}\label{claim:truncated}
	For every $(\wt{\alpha},w)\in \P[{\nn[m]}]$ and $\Ginj:\nn[m]\rightarrow \nn$ such that $\Ginj_*((\wt{\alpha},w))=(\wt{\beta},z)$, there exists a unique morphism $\Ginj[g]:\nn^t\rightarrow\nn[m]$ such that
	$$
	\Ginj[g]_*:(\wt{\beta}^t,z)\mapsto (\wt{\alpha},w)\quad \text{and } \quad \Ginj^t = \Ginj\circ \Ginj[g].
	$$
Diagrammatically,
$$
\xymatrix{
	 & (\wt{\beta},z) \\
 (\wt{\beta}^t,z) \ar[ru]^{\Ginj^t} \ar@{-->}[r]^{\exists !} & (\wt{\alpha},w) \ar[u]
}
$$
	In particular, any $G$-injection $\Ginj$ that sends one truncated element to another is an isomorphism.
\end{claim}
This will be proved below, but first we use the claim to prove that $\P[\bullet]$ is a disjoint union of orbits of truncated elements, and that every such orbit is an induced $\fig$-set.

Indeed, fix a truncated element $x=x^t\in \P[{\nn[m]_0}]$. There is a
tautological surjection from the induced $\fig$-set
$\Hom_{\fig}(\nn[m]_0,\bullet)\times_{\operatorname{Stab}_{x}}\{x\}$
onto the $\fig$ orbit of $x$. To see that this is also injective, suppose
that for $i=1,2$ the $G$-injections $\Ginj[f]_i:\nn[m]_0\rightarrow \nn[N]$ map
$x$ to the same element $y$. Then by Claim \ref{claim:truncated} there exist $\Ginj[g]_i:\nn[N]^t\rightarrow \nn[m]_0$ such that
$$
\Ginj[g]_i:y^t\mapsto x \quad \text{and } \quad \Ginj[f]_1\circ \Ginj[g]_1 = \Ginj[f]_2\circ \Ginj[g]_2.
$$
But since $x=x^t$ is already truncated, Claim \ref{claim:truncated} shows that the $\Ginj[g]_i$'s are isomorphisms. Therefore
$$
\Ginj[f]_1 = \Ginj[f]_2\circ \Ginj[g]_2\Ginj[g]_1^{-1} \quad \text{ and } \quad \Ginj[g]_2\Ginj[g]_1^{-1}:x\mapsto x
$$
so the product $\Ginj[g]_2\Ginj[g]_1^{-1}$ belongs to $\operatorname{Stab}_x$ and the two pairs $(\Ginj[f]_1,x)=(\Ginj[f]_2,x)$ in $\Hom_{\fig}(\nn[m]_0,\bullet)\times_{\operatorname{Stab}_{x}}\{x\}$.

To show that these orbits give a disjoint union decomposition of $P[\bullet]$ suppose that $x'$ is another truncated element, and the orbits $\fig(x)\cap \fig(x') \neq \emptyset$. Then by Claim \ref{claim:truncated} it follows that $x$ and $x'$ belong to each other's orbit, and therefore the two orbits are equal.

Since every element $(\wt{\beta},z)$ has a truncation $\Ginj^t_*:(\wt{\beta}^t,z)\rightarrow (\wt{\beta},z)$, this shows that we exhaust all of $\P[\bullet]$ in this way and completes the proof. 
\end{proof}

\begin{proof}[Proof of Claim \ref{claim:truncated}]
	Recall that the action of a $G$-injection $\Ginj=(\bar{g},f):\nn[m]\rightarrow \nn$ on a $G$-partition is by adjoining singletons of all elements not hit by $f$ (along with some relabeling of elements). By definition, the truncation removes all singletons, and therefore $\nn^t$ contains only elements of $\nn$ that are hit by $f$. Since $f$ is an injection, it has a unique right inverse $\nn^t \overset{f^{-1}}{\longrightarrow} \nn[m]$. Composing this function with $\bar{g}:\nn[m]\rightarrow G$ gives a $G$-labeling of $\nn^t$ from which we get the desired $G$-injection:
	$$
	(\bar{g}^{-1}\circ f^{-1}, f^{-1}):\nn^t \rightarrow \nn[m].
	$$
	Checking that this satisfies the claimed properties is easy.
\end{proof}

Lastly, we include a useful construction on $\fig$-sets that will be used in cohomology calculations below. Many computations involving the Whitney homology include decorations of the $\fig$-poset. The following `welding' construction gives a uniform way of tacking on additional data to a projective $\fig$-set while keeping the projectivity.
\begin{lemma}
	There exists a natural `welding' functor $\langle-|-\rangle: (\fig-\Set)^2 \rightarrow \fig-\Set$ that merges projectives into projectives: if $S_i$ is a $\G[{n_i}]$-set for $i=1,2$ then
	\begin{eqnarray*}
	\langle \Ind_{n_1}(S_1)| \Ind_{n_2}(S_2)\rangle &\cong & \Hom_{\fig}(\nn_1 \ip \nn_2, \bullet)\times_{(\G[{n_1}]\times \G[{n_2}])} S_1\boxtimes S_2 \\
	 &\cong& \Ind_{n_1 + n_2} \left( \G[{n_1+n_2}] \times_{(\G[{n_1}]\times \G[{n_2}])} S_1\boxtimes S_2\right).
	\end{eqnarray*}
	Note that generation degree sums.
\end{lemma}
\begin{proof}
Recall that given two $\fig$-sets $S_\bullet$ and $T_\bullet$, one can form their external product $(S\boxtimes T) _{\bullet_1,\bullet_2}:= S_{\bullet_1}\times T_{\bullet_2}$, which is now an $\fig^2$-set.

By abstract nonsense, the disjoint union functor $\ip: \fig^2\rightarrow \fig$ induces an adjunction $\ip_! \vdash \ip^*$ on $\fig$-sets. We claim that the composition $\ip_!(-\boxtimes -)$ is the desired welding functor.

First observe that
	$$
	\Ind_{a}(S)\boxtimes \Ind_{b}(T) \cong \Ind_{(a,b)}(S\boxtimes T)
	$$
	as $\fig^2$-sets -- this follows easily from the definitions. Now, for every $\fig$-set $M$ the adjunction gives natural isomorphisms
	\begin{eqnarray*}
		\Hom_{\fig-\Set}\left( \ip_!\left(\Ind_{(a,b)}(S\boxtimes T) \right), M \right) &\cong& \Hom_{\fig^2-\Set}\left( \Ind_{(a,b)}(S\boxtimes T), M_{\ip(\bullet)} \right) \\
		&\cong& \Hom_{\G[a]\times \G[b]} \left( S\boxtimes T , M_{a\ip b} \right)
	\end{eqnarray*}
	while the latter set is also naturally isomorphic to
	$$
	\Hom_{\fig} \left(  \Hom_{\fig}(a\ip b, \bullet)\times_{\G[a]\times \G[b]}(S\boxtimes T), M  \right)
	$$
	Thus the two $\fig$-sets in question are naturally isomorphic, as claimed.
\end{proof}

\subsection{Stability in the cohomology of $\M[\bullet](G,X)$}
Our main goal in this section is to show that the sequence $\Ho^i(\M(G,X))$ exhibits representation stability. In \ref{subsec:cohomology} we discussed a spectral sequence computing \EEE SOME WORDS OF INTRODUCTION.

\begin{theorem}[\textbf{Stability of the $E_1$-page}]\label{thm:cohomological_stability}
	Let $F$ be a field of coefficients. Then there is a spectral sequence of $\fig$-modules computing $\Ho_c^*(\M[\bullet](G,X);F)$ in which every term $E_1^{p,q}(\bullet)$ is a finitely generated projective $\fig$-module with generators in degrees $\leq 2p+q$.
\end{theorem}
The general theory of representation stability allows one to turn the pages of the spectral sequence while preserving the sense in which the sequence of representations stabilizes.
\begin{corollary}
	The sequence $\Ho^i(\M[\bullet](G,X);F)$ exhibits representation stability in the following sense:
	\begin{itemize}
		\item When $\operatorname{char}(F)=0$ the sequence is uniformly representation stable in the sense of \cite{CF}, with (SAY EXPLICIT THINGS ABOUT INJECTIVITY AND SURJECTIVITY).
		\item When $\operatorname{char}(F)=p>0$ the sequence (SHOWS PERIODICITY? NEED TO CHECK WITH ROHIT).
	\end{itemize}
\end{corollary}

Plan:
\begin{enumerate}
\item Give concrete consequences to the cohomology of our arrangements.
\end{enumerate}
SAY THIS NICELY:
In particular, the characters $\chi_{\Ho^i(\M)}$ are given by a fixed character polynomial for all $n>??$; the Betti number $b_i(n)$ is polynomial in $n$ once $n\geq ??$.
+ The invariants in $char(F)=0$ are the cohomology of the quotient, and it has homological stability. (what's new about this? is anything new?).

SAY WHAT IS NEW ABOUT THIS AND COULD NOT HAVE BEEN PROVED BEFORE.
\subsection{Proof of Theorem ??}
Let $F$ be a fixed field of coefficients. From \cite{BG}[Theorem ???], Petersen's spectral sequence for $\M(G,X)$ has
\[E_1^{pq}(n) = \bigoplus_{\beta\in \D^p(G,S)} \wt{H}_{p-2}(\zero,\beta)\otimes H_q^{BM}(X_\beta;F).\]

Naturality of this spectral sequence implies that each $E_1^{p,q}(\bullet)$ term is an $\fig$-module, where a $G$-injection $\Ginj:\nn\rightarrow\nn[m]$ induces a map on the summands by its action on layers $\Ginj_*:\D(G,S)\rightarrow\D[m](G,S)$: explicitly, on the poset homology term
	$$
	\Ginj_*:(\zero,\beta) \overset{\sim}{\rightarrow} (\zero, \Ginj_*(\beta)) \text{ induces }\; \Ginj_*:\wt{H}_{p-2}(\zero,\beta)\overset{\sim}{\rightarrow} \wt{H}_{p-2}(\zero,\Ginj_*(\beta))
	$$
	while on the geometric term
	$$
	\xymatrix{
	X_{\Ginj_*(\beta)} \ar[d]_{\Ginj^*} \ar@{^{(}->}[r] & X^{\nn[m]} \ar[d]^{\Ginj^*} \\
	X_{\beta} \ar@{^{(}->}[r] & X^{\nn}
}
\text{ induces }\; 
	\xymatrix{
	\Ho^{BM}_q(X_{\Ginj_*(\beta)})  & \ar[l] \Ho^{BM}_q(X^{\nn[m]}) \\
	\Ho^{BM}_q(X_{\beta}) \ar[u]_{\Ginj_*} & \ar[l] \Ho^{BM}_q(X^{\nn}) \ar[u]^{\Ginj^*}.
} 
$$
Our goal is to show that each $E_1^{p,q}(\bullet)$ is projective with finitely many generators subject to the specified degree bounds. For every truncated element $\beta^t$ of $\D[\bullet]^p(G,S)$ let $\fig(\beta^t)_\bullet$ be the induced $\fig$-set that it generates, and let
$$
M(\beta^t)_\bullet = \bigoplus_{\beta\in \fig(\beta^t)_\bullet} \wt{H}_{p-2}(\zero,\beta)\otimes H_q^{BM}(X_\beta;F).
$$
Theorem \ref{thm:combinatorial_stability} shows that $\D[\bullet](G,S)$ is generated by finitely many of its truncated elements, thus it is clear that $E_1^{p,q}(\bullet)$ splits as a finite direct sum of $M(\beta^t)_\bullet$. It will suffice to show that $M(\beta^t)_\bullet$ is projective with generator of degree $\leq 2p+q$.

Fix $\beta^t\in \D[\nn^t](G,S)$. Then for every $\Ginj=(\bar{g},f): \nn^t\rightarrow \nn$ the partition of $\Ginj_*(\beta^t)=\beta$ is gotten by adjoining the elements of $\nn\setminus f(\nn^t)$ to $\beta^t$ as singletons. Thus
$$
X_{\beta} \cong X_{\beta^t}\times \underbrace{X\times \ldots \times X}_{\text{product over }\nn\setminus f(\nn^t)}
$$
and by the Kunneth formula
$$
\Ho^{BM}_{q}(X_\beta;F) = \bigoplus_{r+\sum_{i\in \nn \setminus f(\nn^t)} \alpha_i = q} \Ho^{BM}_{r}(X_{\beta^t};F) \otimes \left( \bigotimes_{i\in \nn\setminus f(\nn^t)} \Ho^{BM}_{\alpha_i}(X;F) \right) 
$$



\comment{spectral sequence stuff. cite
\cite{Petersen2017,Church2012,Casto2016,bibby2,Totaro1996}.}


\bibliographystyle{alpha}
\bibliography{paper}

\end{document}
